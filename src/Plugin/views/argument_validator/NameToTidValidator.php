<?php

namespace Drupal\name_to_tid_validator\Plugin\views\argument_validator;


use Drupal\views\Plugin\views\argument_validator\ArgumentValidatorPluginBase;

/**
 * Validate the New boolean.
 *
 * @ingroup views_argument_validate_plugins
 *
 * @ViewsArgumentValidator(
 *   id = "name_to_tid_validator",
 *   title = @Translation("Name to TID Validator")
 * )
 */
class NameToTidValidator extends ArgumentValidatorPluginBase {

  /**
   * @param $argument
   *
   * @return bool
   */
  public function validateArgument($argument) {
    // This replaces dashes in the URL to spaces in the term name
    // but will not validate if your term name contains a dash already.
    // @todo: 'Implement a more sophisticated solution'
    if (strpos($argument, '-')) {
      $argument = str_replace('-', ' ', $argument);
    }
    // Use query to be able to get the TID no matter what language
    // the term is supplied in.
    $query = \Drupal::database()->select('taxonomy_term_field_data', 'td');
    $query->addField('td', 'tid');
    $query->condition('td.name', $argument);
    // For better performance, define the vocabulary where to search.
    // $query->condition('td.vid', $vid);
    $term = $query->execute();
    $tid = $term->fetchField();
    if (is_numeric($tid) == TRUE) {
      $this->argument->argument = $tid;
      
      return TRUE;
    } 
    else {
      return FALSE;
    }
  }
}

